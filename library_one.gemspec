Gem::Specification.new do |s|
  s.name        = 'library_one'
  s.version     = '0.4.0'
  s.date        = '2015-11-19'
  s.summary     = "Library One"
  s.description = "A simple library like gem"
  s.authors     = ["Bogdan Hodorog"]
  s.email       = 'bogdan.hodorog@fairfaxmedia.com.au'
  s.files       = ["lib/library_one.rb"]
  s.homepage    =
    'http://doesnt.have.one'
  s.license       = 'MIT'
end
